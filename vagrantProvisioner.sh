#!/usr/bin/env bash

add-apt-repository -y ppa:avsm/ppa
apt-get update

apt-get -y --force-yes install \
  build-essential \
  g++ \
  git-core \
  ocaml \
  opam \
  m4 \
  libtool \
  pkg-config \
  autoconf \
  libshine-dev \
  libfftw3-dev \
  automake \
  unzip \

cd /tmp
wget https://launchpad.net/ubuntu/+archive/primary/+files/libfdk-aac0_0.1.2-1_amd64.deb
dpkg -i libfdk-aac0_0.1.2-1_amd64.deb
wget https://launchpad.net/ubuntu/+archive/primary/+files/libfdk-aac-dev_0.1.2-1_amd64.deb
dpkg -i libfdk-aac-dev_0.1.2-1_amd64.deb

su vagrant <<EOF
cd ~
opam init -a
eval `opam config env`
opam switch 4.02.3
eval `opam config env`
opam install -y depext camlp4
eval `opam config env`
echo '*******************'
echo '*** opam depext ***'
echo '*******************'
yes | opam depext -i -d aacplus
yes | opam depext -i -d \
  base-bytes \
  ao \
  portaudio \
  pulseaudio \
  mad \
  magic \
  camomile \
  taglib \
  lame \
  ogg \
  vorbis \
  opus \
  voaacenc \
  faad \
  flac \
  ladspa \
  samplerate \
  pcre \

eval `opam config env`
echo '**************************'
echo '*** Cloning Liquidsoap ***'
echo '**************************'
cd /tmp
git clone https://github.com/savonet/liquidsoap-full
cd liquidsoap-full
make init
cp PACKAGES.minimal PACKAGES
sed -i "s/#ocaml-portaudio/ocaml-portaudio/g" PACKAGES
sed -i "s/#ocaml-alsa/ocaml-alsa/g" PACKAGES
sed -i "s/#ocaml-pulseaudio/ocaml-pulseaudio/g" PACKAGES
sed -i "s/#ocaml-faad/ocaml-faad/g" PACKAGES
sed -i "s/#ocaml-opus/ocaml-opus/g" PACKAGES
sed -i "s/#ocaml-voaacenc/ocaml-voaacenc/g" PACKAGES
sed -i "s/#ocaml-fdkaac/ocaml-fdkaac/g" PACKAGES
sed -i "s/#ocaml-aacplus/ocaml-aacplus/g" PACKAGES
sed -i "s/#ocaml-ladspa/ocaml-ladspa/g" PACKAGES
sed -i "s/#ocaml-samplerate/ocaml-samplerate/g" PACKAGES

./bootstrap
./configure --with-user=vagrant --with-group=vagrant
make
EOF

cd /tmp/liquidsoap-full
make install

mkdir /var/log/liquidsoap
chmod 777 /var/log/liquidsoap
 