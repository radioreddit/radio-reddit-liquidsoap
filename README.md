# Radio Reddit Liquidsoap Config
*This project is no longer maintained.*

These were the Liquidsoap config files for running RadioReddit.com.

![Channels and Streams](https://bitbucket.org/radioreddit/radio-reddit-liquidsoap/raw/master/doc/prodChannelsAndStreams.svg)

## Getting Started
This repo has a Vagrantfile which will build and install Liquidsoap and all of its dependencies.  Simply
`vagrant up` to get started.  The provisioner takes around 15 minutes to run.  To save time on future use, it is
recommended to use `vagrant halt` to shut down your box rather than destroying it.

Once running, use `vagrant ssh`, `cd /vagrant` and then `liquidsoap main.liq` or similar to run Liquidsoap.

Only STDERR will be output on your console.  If you want to see normal logging output, you must tail the logs
located at `/var/log/liquidsoap`.

## Production Installation

These Liquidsoap files are installed to three boxes:

 - rrssource1.radioreddit.com
 - rrssource2.radioreddit.com
 - rrssource3.radioreddit.com
 
Each box takes 2 or 3 streams.  Any more than that, and the boxes get overloaded.  The repo is installed
to `/opt/radio-reddit-liquidsoap`.  In `/etc/liquidsoap`, there are files that `include` the files from
`/opt/radio-reddit/liquidsoap`.  For instance, in `/etc/liquidsoap/talk.liq` on rrssource3:

```
%include "/opt/radio-reddit-liquidsoap/talk.liq"
```

A symlink does not work due to the interesting way that Liquidsoap sets up paths to include later files.

## License
Copyright 2016, Radio Reddit, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
